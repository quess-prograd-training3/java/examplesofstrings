import com.example.StringOperations;

public class Main {
    public static void main(String[] args) {

        StringOperations stringOperations=new StringOperations();
        stringOperations.initializeString();
        stringOperations.compareStrings();
    }
}